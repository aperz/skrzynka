#!/usr/bin/env python3


import subprocess
import pandas as pd
import numpy as np
import os
import seaborn as sns

from sklearn.feature_selection import VarianceThreshold, SelectKBest, f_regression, mutual_info_regression, f_classif, mutual_info_classif
from sklearn.decomposition import PCA
from sklearn.model_selection import GridSearchCV, train_test_split
from sklearn.pipeline import Pipeline
from sklearn.svm import LinearSVC

import matplotlib.pyplot as plt

from preprocess import *
from skrzynka.skrzynka import *


def compare_dim_red_methods(X, y, mode='classif'):
    '''
    http://scikit-learn.org/stable/auto_examples/plot_compare_reduction.html#sphx-glr-auto-examples-plot-compare-reduction-py
    '''

    pipe = Pipeline([
        ('reduce_dim', PCA()),
        ('classify', LinearSVC())
    ])

    N_FEATURES_OPTIONS = [1, 2, 4, 8, 10, 60]
    C_OPTIONS = [1, 10, 100, 1000]
    if mode == 'regression':
        param_grid = [
            {
                'reduce_dim': [PCA(iterated_power=7)],
                'reduce_dim__n_components': N_FEATURES_OPTIONS,
                'classify__C': C_OPTIONS
            },
            {
                'reduce_dim': [SelectKBest(f_regression)],
                'reduce_dim__k': N_FEATURES_OPTIONS,
                'classify__C': C_OPTIONS
            },
            {
                'reduce_dim': [SelectKBest(mutual_info_regression)],
                'reduce_dim__k': N_FEATURES_OPTIONS,
                'classify__C': C_OPTIONS
            },
        ]

    if mode == 'classif':
        param_grid = [
            {
                'reduce_dim': [PCA(iterated_power=7)],
                'reduce_dim__n_components': N_FEATURES_OPTIONS,
                'classify__C': C_OPTIONS
            },
            {
                'reduce_dim': [SelectKBest(f_classif)],
                'reduce_dim__k': N_FEATURES_OPTIONS,
                'classify__C': C_OPTIONS
            },
            {
                'reduce_dim': [SelectKBest(mutual_info_classif)],
                'reduce_dim__k': N_FEATURES_OPTIONS,
                'classify__C': C_OPTIONS
            },
        ]
    grid = GridSearchCV(pipe, cv=5, n_jobs=8, param_grid=param_grid)

    if mode == 'regression':
        reducer_labels = ['PCA', 'KBest(f_regression)', 'KBest(mi_regression)']
    if mode == 'classif':
        reducer_labels = ['PCA', 'KBest(f_classif)', 'KBest(mi_classif)']

    grid.fit(X, y)

    mean_scores = np.array(grid.cv_results_['mean_test_score'])
    #std_scores = np.array(grid.cv_results_['std_test_score'])
    # scores are in the order of param_grid iteration, which is alphabetical
    mean_scores = mean_scores.reshape(len(C_OPTIONS), -1, len(N_FEATURES_OPTIONS))
    # select score for best C
    mean_scores = mean_scores.max(axis=0)
    bar_offsets = (np.arange(len(N_FEATURES_OPTIONS)) *
                (len(reducer_labels) + 1) + .5)

    plt.figure()
    COLORS = 'bgrcmyk'
    for i, (label, reducer_scores) in enumerate(zip(reducer_labels, mean_scores)):
        plt.bar(bar_offsets + i, reducer_scores, label=label, color=COLORS[i])

    plt.title("Comparison of feature reduction methods")
    plt.xlabel('Number of features')
    plt.xticks(bar_offsets + len(reducer_labels) / 2, N_FEATURES_OPTIONS)
    if mode == 'regression':
        plt.ylabel('Regression accuracy')
    if mode == 'classif':
        plt.ylabel('Classification accuracy')
    plt.ylim((0, 1))
    plt.legend(loc='upper left')
    plt.show()
