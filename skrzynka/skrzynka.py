#!/usr/bin/env python3


'''
Helpful tools for data manipulation and the like.
Should not import any of the other modules that I created.
'''

import itertools

def notes_toolbox():
    print("NOTES FOR toolbox.py:\n\
        import time.time()\n\
        t0 = time()\n\
        print('done in %fs' % (time() - t0))\n\
        ")

#import argparse
import subprocess as sp
import pandas as pd
import numpy as np
import sys
from collections import Counter
import collections
import re
import os
from difflib import SequenceMatcher
from bs4 import UnicodeDammit

SAM_MD_FP = '/D/ebi/DEFAULT_METADATA.tsv'
TAX_MD_FP = '/D/ebi/taxonomy_metadata.tsv'

# -------
# options
# -------

class Options():
    def __init__(self, **kwargs):
        self.__dict__.update(**kwargs)

    def addop(self, **kwargs):
        self.__dict__.update(**kwargs)

    def delop(self, *args):
        for i in args:
            try:
                self.__dict__.pop(i)
            except KeyError:
                print(str(i), "was not defined.")

    def print_options(self, *args):
        return self.__dict__



# ----
# misc
# ----

def goo():
    print('hjk789989789')
    print(__name__)

def history2ipynb():
    print(':%s/$/\\n",/g')
    print(':%s/^/"/g')

def ipynb2history():
    print('ni')

def import_all_from(module_list):
    for m in module_list:
        exec('from ' + m + 'import *')


def eAnd(*args):
    '''
    Returns a bool (all()) for each pair in an iterable
    '''
    return [all(tuple) for tuple in zip(*args)]

def timeout(func, args=(), kwargs={}, timeout_duration=1, default=None):
    import signal

    class TimeoutError(Exception):
        pass

    def handler(signum, frame):
        raise TimeoutError()

    # set the timeout handler
    signal.signal(signal.SIGALRM, handler)
    signal.alarm(timeout_duration)
    try:
        result = func(*args, **kwargs)
    except TimeoutError as exc:
        result = default
    finally:
        signal.alarm(0)

    return result

# ----------
# data types
# ----------

# infer date from differently formatted date entries
def human2date(s):
    import dateparser
    if not hasattr(s, '__iter__') or isinstance(s, str):
        s = [s]
    new = []
    failed = []
    default = np.nan

    for i in s:
        print('--')
        print(i)

        if i in failed:
            #print(True)
            i_ = default

        else:
            #print(False)

            try:
                #i_ = dateparser.parse(i)
                i_ = timeout(dateparser.parse, kwargs={'date_string':i},
                            timeout_duration=1, default = default)

                if i_ == default:
                    failed.append(i)
                if pd.isna(i_):
                    failed.append(i)
                if pd.isnull(i_):
                    failed.append(i)
                if str(i_) == 'None':
                    failed.append(i)

            except TypeError:
                i_ = default
                failed.append(i)

        print(i_)
        new.append(i_)

        #print(failed)

    return new, failed



# ------------
# pandas utils
# ------------
def rename_duplicate_index(df, axis=0):
    if axis==0:
        ix = pd.Series(df.index)
        for dup in df.index.get_duplicates():
            ix[df.index.get_loc(dup)] =\
                    [dup+'_'+str(d_idx) if d_idx!=0 else dup
                    for d_idx in range(df.index.get_loc(dup).sum())]
        df.index=ix
    if axis==1:
        ix = pd.Series(df.columns)
        for dup in df.index.get_duplicates():
            ix[df.columns.get_loc(dup)] =\
                    [dup+'_'+str(d_idx) if d_idx!=0 else dup
                    for d_idx in range(df.columns.get_loc(dup).sum())]
        df.columns=ix

    return df


def summ(*args, what="shape"):
    '''
    Prints out information for multiple pandas DataFrames:
    shape, columns, or index
    '''

    if what == "shape":
        return [i.shape for i in args]
    if what == "columns":
        return [i.columns for i in args]
    if what == "index":
        return [i.index for i in args]


# ------------
# string utils
# ------------

def pluralize(label):
    if label in ['taxa', 'taxon']:
        return 'taxa'
    elif label[-1] == 's':
        return label
    else:
        return label+'s'


def names_to_human_readable(names, capitalize_first=True):
    #names = [str(n) for n in names]
    is_string = False
    if isinstance(names, str):
        is_string = True
        names = [names]

    if capitalize_first:
        o = [str(n).replace('_', ' ') for n in names]
        o = [n[0].upper()+n[1:] for n in o]
    else:
        o = [n.replace('_', ' ') for n in names]

    capitalize = ['id']
    for c in capitalize:
        #o = [n.replace(c, c.upper()) for n in o]
        o = [n.upper() if n.lower() == c else n for n in o]

    if is_string:
        o = o[0]

    return o


def progress(length, nchunks=4):
    steps = [i for i in range(0, length, int(length/nchunks))]
    steps.append(length)
    chunks = [1/nchunks*i for i in range(len(steps))]
    return steps, chunks

    #steps = zip(steps, chunks)
    #for i,sid in enumerate(samples):
    #    if i > steps[0]:
    #        print("--- --- "+str(chunks[0]*100) + "% complete")
    #        steps, chunks = steps[1:], chunks[1:]


def import_all_from(module_list):
    for m in module_list:
        exec('from ' + m + ' import *')


def similar(a,b):
    return SequenceMatcher(None, a,b).ratio()

# ------------
# pandas utils
# ------------


def dummy_df(dim=(10,7), distort=False,
            col_prefix='c', row_prefix='i', add_cat= True,distort_by_cat=True,
            dists = ['uniform', 'normal', 'logistic', 'lognormal'],#, 'negative_binomial'],
            ):
    # dist ['uniform', 'normal']
    A = pd.DataFrame(
            np.zeros(dim),
            columns = [str(col_prefix)+str(i) for i in range(dim[1])],
            index = [str(row_prefix)+str(i) for i in range(dim[0])],
        )

    for d in dists:
        A.insert(0, str(d),
                [round(i,2) for i in getattr(np.random, d)(size=dim[0])])

    if distort:
        i=1
        for c in A:
            A[c] = A[c]+i
            i+=1

    if add_cat:
        A.insert(0, 'cat0', np.random.choice(['a','b','c'], size=A.shape[0]))

        def fun1(x):
            if x.dtypes in [float, int]:
                return x+np.random.randint(1,20)
            else:
                return x

        if distort_by_cat:
            A = A.groupby('cat0').apply(lambda x: x.apply(fun1))

    return A


def insert_dummy_columns(df, colnames):
    '''
    colnames : list or pandas.Index
    '''
    missing_cols = set(colnames).difference(set(df.columns))
    for mc in missing_cols:
        df.insert(0, mc, "NaN")
    return df

def query_rows(df, query):
    '''
    df    a pandas df
    Returns:    a subset of cells that match the pattern

    Shows partial matches
    '''
    #TODO: add fuzzy=True arg

    if isinstance(df, pd.core.frame.DataFrame):
        o = {}
        for c in df.columns:
            if not df[c].dtype in ['int64', 'float64', 'bool']:
                o[c] = df[c].str.contains(query, regex=True, flags=re.IGNORECASE)
            else:
                o[c] = [False for i in range(df.shape[0])]
        o = pd.DataFrame(o)
        o.fillna(False, inplace=True)

        return df.ix[o.apply(any, 1), :]


def query_iter(iterable, query, return_subset=True):
    '''
    iterable    a pandas df or list

    Returns:    a subset of cells that match the pattern

    Shows partial matches
    '''
    #TODO: add fuzzy=True arg

    if isinstance(iterable, (list, pd.indexes.base.Index)):
        o = pd.Series(iterable).str.contains(query, regex=True, flags=re.IGNORECASE)

        if return_subset:
            o = pd.Series(iterable)[o]

    if isinstance(iterable, pd.core.frame.DataFrame):
        o = {}
        for c in iterable.columns:
            if not iterable[c].dtype in ['int64', 'float64', 'bool']:
                o[c] = iterable[c].str.contains(query, regex=True, flags=re.IGNORECASE)
            else:
                o[c] = [False for i in range(iterable.shape[0])]
        o = pd.DataFrame(o)
        o.fillna(False, inplace=True)

        if return_subset:
            o = subset_with_mask(iterable,o)

    return o



def subset_with_mask(df, mask):
    return df.ix[mask.apply(any, 1), mask.apply(any, 0)]


def pandas_to_SE_table(df, ofile):
    with open(ofile, 'w') as o:
        o.write("\n".join(['    '+i for i in df.to_string().split('\n')]))


def delete_empty(A):
    '''
    Removes columns and rows which sum is 0
    Alse removes all NaN and all None columns and rows
    '''
    A = A.loc[:, list(A.sum(0) > 0)]
    A = A.loc[list(A.sum(1) > 0), :]
    return A


def delete_low_appearence(A, min_appear=20, axis="both"):
    assert axis in ["both", 0,1]
    if axis == "both":
        return A.loc[
            (A.applymap(bool).sum(1)>min_appear),
            (A.applymap(bool).sum(0)>min_appear)
            ]
    elif axis == 0:
        return A.loc[
            (A.applymap(bool).sum(1)>min_appear), :
            ]
    elif axis == 1:
        return A.loc[
            :, (A.applymap(bool).sum(0)>min_appear)
            ]

def delete_low_sd(A, min_std=0.2, axis='both'):
    assert axis in ["both", 0,1]
    if axis == "both":
        A = A.loc[A.std(1) > min_std, :]
        A = A.loc[:, A.std(0) > min_std]
    elif axis == 0:
        A = A.loc[A.std(1) > min_std, :]
    elif axis == 1:
        A = A.loc[:, A.std(0) > min_std]
    return A

def permute_pd(A, axis=1):
    return pd.DataFrame(A.sample(A.shape[axis], axis=axis).values,
                            index=A.index, columns = A.columns)

def permute_dist_pd(A):
    '''
    Takes in a distance matrix and returns a permuted matrix without
    breaking the symmetry. (checked)
    '''
    new_index = np.random.permutation(A.index)
    A = A.reindex(new_index, new_index)
    return A


def binarize_column(A, colname):
    print('pd.get_dummies!')

def subset_treshold(A, mi=None, ma=None):
    if isintance(mi, (float, int)):
        A = A.applymap(lambda x: x if x >= mi else None)
    if isintance(ma, (float, int)):
        A = A.applymap(lambda x: x if x <= ma else None)

    return delete_empty(A)

def sam_or_tax(colname, sam_md_fp=SAM_MD_FP, tax_md_fp=TAX_MD_FP):
    sc = pd.read_csv(sam_md_fp, sep='\t', nrows=0).columns
    tc = pd.read_csv(tax_md_fp, sep='\t', nrows=0).columns
    assert len(set(sc).intersection(tc)) == 0, 'There is overlap in metadata tables column names.'
    if colname in sc:
        return sam_md_fp
    if colname in tc:
        return tax_md_fp


def collM(M_or_fp = 'data/dummy_A.tsv', collapse_rows = None, collapse_cols = None,
            primary_levels = ('sample_id', 'taxonomy'), method=np.sum,
            #sam_md_fp = SAM_MD_FP, tax_md_fp = TAX_MD_FP,
            ):
    '''
    Collapse matrices by row or column index!
    '''
    # TODO PARALLELIZE!!

    if isinstance(M_or_fp, str):
        A = pd.read_csv(M_or_fp, index_col=0, sep='\t')
    else:
        A = M_or_fp

    A.columns.name = primary_levels[1]
    A.index.name = primary_levels[0]

    if (collapse_rows, collapse_cols) == (None, None):
        return A

    if collapse_rows != None:
        if collapse_rows == primary_levels[0]:
            return A
        md = pd.read_csv(sam_or_tax(collapse_rows), sep='\t', index_col=False,
                            usecols = [collapse_rows, primary_levels[0]])
        assert collapse_rows in md.columns
        A = add_index_level(A, md, primary_levels[0], collapse_rows, axis=0)
        A = A.groupby(level=collapse_rows).apply(method)

    if collapse_cols != None:
        if collapse_cols == primary_levels[1]:
            return A
        md = pd.read_csv(sam_or_tax(collapse_cols), sep='\t', index_col=False,
                            usecols = [collapse_cols, primary_levels[1]])
        assert collapse_cols in md.columns
        A = A.T
        A = add_index_level(A, md, primary_levels[1], collapse_cols, axis=0)
        A = A.groupby(level=collapse_cols).apply(method)
        A = A.T

    return A


def vectorize_pd(A):
    return 'deprecated; use A.values.ravel()'

def vectorize_np(A):
    return 'deprecated; probably not used anywhere'


### biom stuff
def add_columns_from(from_md, target_md, col_name="run_id", col_on = "sample_id"):
    return pd.merge(from_md[[col_on, col_name]], target_md, on=col_on)


def restore_keywords_from_metadata(md, keyword_colname, sep=" "):
    return list(set(
                flatten(
                        [i.split(sep) for i in md[keyword_colname]\
                        .drop_duplicates().tolist()
                    if isinstance(i, str)])
            ))


def mapp(md, index_colname="project_id", col_list=["sample_id"]):
    '''
    index_colname (str): name of md column to be used as index
    col_list (list of str): names of columns to be returned from md
    returns: a pandas.DataFrame
    '''
    col_list = col_list +[index_colname]
    md = md[col_list].drop_duplicates()
    md.index = md[index_colname]
    md.index = md.index.to_native_types()
    o = md.drop(index_colname, 1)
    return o

def mx_for_prj(A, md, prj):
    '''
    A:          A_OTU-TSV
    returns:    matrix with samples only from specified project
    '''
    try:
        ss = mapp(md).ix[prj].sample_id.drop_duplicates().tolist()
        ss = [sam for sam in ss if sam in A.index.tolist()]
        o = A.ix[ss,:]
        o = o.ix[:, o.sum(0) != 0]
    except AttributeError as e:
        print("--- ---", e, "- That means no samples found in the data for project", prj)
        o = pd.DataFrame()

    return o  # may return empty frame


def add_index_level(M, mapping, existing_level, new_level,
                    axis=0, return_index=False):
    '''
    axis : either 0 (rows), 1, or 'both'
    '''
    # TODO accept index object and return multiindex

    if axis == 1:
        M=M.T

    axes_names = (M.index.names, M.columns.names)
    mi = mapping[[existing_level, new_level]].drop_duplicates()
    mi.index = mi[existing_level]
    mi = mi[[new_level]]

    M, mi = M.align(mi, level=0, axis = 0, join='inner')
    mi = mi.set_index(new_level, append=True).index

    if return_index:
        return mi

    M.index = mi
    M = M.reorder_levels([new_level, existing_level])

    if axis == 1:
        M=M.T

    if axis == 'both':
        M = add_index_level(M=M, mapping=mapping,
                            existing_level=existing_level,
                            new_level=new_level,
                            axis=1, return_index=return_index
                            )

    return M

def select_columns_matching(X, pattern, reverse=False):
    '''
    X           a pandas DataFrame
    pattern     str or list of str
    '''
    #TODO: use str.contains or else use re which provides more functionality
    if isinstance(pattern, list):
        o = []
        if not reverse:
            for p in pattern:
                o.append(X.ix[:, (X.columns.str.find(p) != -1)])
        else:
            for p in pattern:
                o.append(X.ix[:, [not i for i in (X.columns.str.find(p) != -1)]])
        return pd.concat(o, axis=1, join='outer')
    else:
        if not reverse:
            return X.ix[:, (X.columns.str.find(pattern) != -1)]
        else:
            return X.ix[:, [not i for i in (X.columns.str.find(pattern) != -1)]]

def merge_data_frames(dfs, **kwargs):
    from functools import reduce
    merged = reduce(
        lambda left,right: pd.merge(left,right, **kwargs), dfs
        )
    return merged

# -----------------------
# file management: output
# -----------------------


def table_writer(o_fp, colnames, d = None, initiate=False):
    #TODO open file outside of function and handle as arg
    '''
    d : dict, pandas.core.frame.DataFrame (TODO: or list)
    initiate : bool; if True, only wrote the header. d is not used
    '''
    if initiate:
        # clean file and write header
        open(o_fp, 'w').close()
        header = "\t"+"\t".join([str(i) for i in colnames])+"\n"
        with open(o_fp, 'a') as o:
            o.write(header)
        return 'Header written to file'

    else:
        if isinstance(d, dict):
            dict2table_writer(o_fp, colnames, d)
        if isinstance(d, pd.core.frame.DataFrame):
            pandas2table_writer(o_fp, colnames, d)
        else:
            raise Exception('d is a', str(type(d)),'and is not a valid object')


def pandas2table_writer(o_fp, colnames, d):
    if d.shape[0] > 0:
        if not all([i in colnames for i in d.columns.tolist()]):
            # if there are colnames in d.columns that are not in colnames:
            print(d.columns)
            print(colnames)
            if len(set(d.columns).difference(set(colnames))) != 0:
                raise Exception('DataFrame (d) has column names that are not in listed colnames.')
            # if there are columns in colnames that are not in d.colums
        else:
            d = insert_dummy_columns(d, colnames)
            d = d[colnames]

        with open(o_fp, 'a') as o:
            s = d.to_csv(sep='\t', header=False)
            o.write(s)


def dict2table_writer(o_fp, colnames, d):
    '''
    Write a table with specified colnames line by line into a file
    Fill in NaNs
    appending : bool whether appending the file (do not write header)
    '''

    with open(o_fp, 'a') as o:
        #TODO check if colnames is same as first line in file
        for k,v in d.items():
            line = []
            for c in colnames:
                if c in v.keys():
                    line.append(v[c])
                else:
                    line.append("NaN")

            line = str(k)+"\t"+"\t".join([str(i) for i in line])+"\n"
            o.write(line)


# ----------------------
# file management: input
# ----------------------

def decode_bytes(b, errors='ignore'):
    ### NOTES
    # b=b"Sacr\xc3\xa9 bleu!"
    # <3
    #wrapped_stdout = codecs.getwriter('UTF-8')(sys.stdout.buffer)
    #sys.stdout = wrapped_stdout
    ##
    #content = request.urlopen(url).read()
    #content = decode_bytes(content, errors='ignore')
    ###

    ### OSIRIS seems to hadle this ok - terminal/OS issue!
    # This works:
    s = UnicodeDammit.detwingle(b)
    s = s.decode('utf-8', errors=errors).encode('utf-8').decode()
    ###

    ### UnicodeDammit
#    from bs4 import UnicodeDammit
#    dammit = UnicodeDammit("Sacr\xc3\xa9 bleu!")
#    print(dammit.unicode_markup)
#    dammit.original_encoding
    ###
    #s = UnicodeDammit.detwingle(s)
    #s = s.decode("utf8")
    ###

    ### decode sandwich?
    # This will just delete the offending characters...
    # But will not work for every case! (\xb0)
    #s = s.decode('utf-8', errors=errors).encode('utf-8').decode()
    #s = s.replace("\", "")
    ###

    ### To str()
    # pretty bad. returns a string with a b' and ' !
    #s = str(s)[2:-1]
    ###
    return s


def file2list(fp):
    if isinstance(fp, str):
        entries = [i.strip() for i in open(fp, 'r').readlines()]
    else:
        raise ValueError("Provide a valid path to the terms file.")
    return entries


# ----------
# system; os
# ----------
def mkdir(dirname):
    if not os.path.isdir(dirname):
        os.mkdir(dirname)


def creation_date(path_to_file):
    """
    Try to get the date that a file was created, falling back to when it was
    last modified if that isn't possible.
    See http://stackoverflow.com/a/39501288/1709587 for explanation.
    """
    if platform.system() == 'Windows':
        return os.path.getctime(path_to_file)
    else:
        stat = os.stat(path_to_file)

        try:
            return stat.st_birthtime
        except AttributeError:
            # We're probably on Linux. No easy way to get creation dates here,
            # so we'll settle for when its content was last modified.
            return stat.st_mtime


# ----------------
# dictionary utils
# ----------------


def invert_dict(d):
    return dict( (v,k) for k in d for v in d[k] )

def flatten_dict(d, parent_key='', sep='_'):
    items = []
    for k, v in d.items():
        new_key = parent_key + sep + k if parent_key else k
        if isinstance(v, collections.MutableMapping):
            items.extend(flatten_dict(v, new_key, sep=sep).items())
        else:
            items.append((new_key, v))
    return dict(items)


def fillna_dict(colnames, d):
    '''
    '''
    for k,subd in d.items():
        for c in colnames:
            if not c in subd.keys():
                subd[c] = "NaN"
        for subk,subv in subd.items():
            if subv=="":
                subd[subk] = "NaN"
        d[k] = subd
    return d

def rename_dict_keys(d, mapping):
    for k,v in mapping.items():
        if k in d.keys():
            d[v] = d.pop(k)
    return d

def merge_dicts(list_of_dicts):
    '''
    Assumes keys are unimportant
    '''
    o = {}
    i = 0
    for d in list_of_dicts:
        for v in d.values():
            o[i] = v
            i+=1
    return o

def match_keys(d, s):
    '''
    d   a dictionary whose keys are to be scanned for match (dict)
    s   desired patterns list (list of strings) or string
    '''
    lis = []
    [lis.append(s_) for s_ in s]
    for s_ in lis:
        d = {k:v for k, v in d.items() if str(k).find(s_) != -1}
    return d

def minimize_sum_distance(key_list,val_list, max_dist=None):
            if len(key_list) == 0 or len(val_list) == 0:
                return {}
            reverse = False


            if len(val_list) > len(key_list):
                reverse = True
                vals = key_list
                keys = val_list
            else:
                vals = val_list
                keys = key_list

            perms = [zip(x,vals) for x in itertools.permutations(keys,len(vals))]
            perms = [[i for i in j] for j in perms]
            scores = [sum([np.abs(i[0]-i[1]) for i in j]) for j in perms]
            best = dict(perms[scores.index(min(scores))])

            if max_dist is not None:
                best = {k:v for k,v in best.items() if abs(k-v) <= max_dist}

            if reverse == True:
                best = {v:k for k,v in best.items()}

            return best


# ----------------
# list utils
# ----------------

def flatten(data):
    '''
    Get all leaf nodes from a tree structure
    From saltycrane
    '''
    class Namespace(object):
        pass

    def inner(data):
        if isinstance(data, dict):
            for item in data.values():
                inner(item)
        elif isinstance(data, list) or isinstance(data, tuple):
            for item in data:
                inner(item)
        else:
            ns.results.append(data)

    ns = Namespace()
    ns.results = []
    inner(data)

    return ns.results


def list_sum(list_of_lists):
    if len(list_of_lists) < 2:
        return list_of_lists[0]
    else:
        ll = []
        for l in list_of_lists:
            ll = ll+l
    return ll

def list_intersection(list_of_lists):
    #ll = [set(l) for l in list_of_lists]
    ss = list_sum(list_of_lists)
    return list(set(ll))

