
from pairwise import *
from toolbox import *
import time
from data_management import collM_c
random_state=3


def benchmark_pairwise(sample_size, random_state=random_state, **kwargs):
    '''
    kwargs : to be passed to pairwise_distances_pd
    notably n_jobs and metric
    '''
    A = delete_empty(collM_c().sample(sample_size, random_state=random_state))

    ###
    start = time.time()
    D = pairwise_distances_pd(A, **kwargs)
    end = time.time()
    ###
    print(end-start)
    return D


