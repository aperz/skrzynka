#!/usr/bin/env python3

import subprocess
import pandas as pd
import numpy as np
import argparse
import os
import seaborn as sns
from io import StringIO

from sklearn.feature_selection import VarianceThreshold, SelectKBest, f_regression, mutual_info_regression, f_classif, mutual_info_classif
from sklearn.decomposition import PCA

import matplotlib.pyplot as plt

# rpy2
from rpy2.robjects import r
from rpy2.robjects import pandas2ri
from rpy2.robjects.packages import importr
pandas2ri.activate()

from skrzynka.skrzynka import *

r_results_colnames = ['Df', 'Pillai', 'approx F', 'num Df', 'den Df', 'Pr(>F)',]

# ------------------------
# dimensionality reduction
# ------------------------


def sel_var(X, threshold):
    #d = normalize_pd(d, 1)
    var_filter = VarianceThreshold(threshold = threshold)
    var_filter.fit(X)
    X_sel = var_filter.transform(X)
    o = pd.DataFrame(X_sel, columns = X.columns[var_filter.get_support()],
                        index = X.index)
    variances = pd.Series(var_filter.variances_, index = X.columns)
    return o, variances


def sel_anova(X, y, mode='classif'):
    '''
    "Univariate linear regression tests.
    Quick linear model for testing the effect of a single regressor,
    sequentially for many regressors."

    mode: 'classif' or 'regression'
    '''
    if mode == 'classif':
        anova_filter = SelectKBest(f_classif, k='all')
    if mode == 'regression':
        anova_filter = SelectKBest(f_regression, k='all')

    anova_filter.fit(X, y)
    X_sel = anova_filter.transform(X)
    support = pd.DataFrame(X_sel, columns = X.columns[anova_filter.get_support()],
                        index = X.index)
    pvals = anova_filter.pvalues_
    o = pd.concat([
                    pd.DataFrame(support.columns, columns=['param']),
                    pd.DataFrame(pvals, columns=['pval'])
                ], axis=1)\
            .sort_values('pval')
    return o, support


def sel_mi(X, y, k=10, mode='classif'):
    '''
    Mutual information based feature selection.
    mode: 'classif' or 'regression'
    '''
    if mode == 'classif':
        mi_filter = SelectKBest(mutual_info_classif, k=k)
    if mode == 'regression':
        mi_filter = SelectKBest(mutual_info_regression, k=k)

    mi_filter.fit(X, y)
    X_sel = mi_filter.transform(X)
    o = pd.DataFrame(X_sel, columns = X.columns[mi_filter.get_support()],
                        index = X.index)
    pvals = mi_filter.pvalues_
    return o, pvals



def get_pc(d, **kwargs):
    pca = PCA(**kwargs)
    pca.fit(d.T)
    colnames = ['PC'+str(n) for n in range(1,pca.components_.shape[0]+1)]
    comps = pd.DataFrame(pca.components_.T, columns = colnames, index = d.index)
    exp_var = pd.Series(pca.explained_variance_ratio_, index = colnames)

    return comps, exp_var, pca


# -----
# tests
# -----

def anovaR(data_fp, formula):
    tmpfile='.tmpvacou'
    #if not os.path.isfile(tmpfile):
        #os.mknod(tmpfile)
    #r.assign('d', data)
    r("d <- read.csv('"+data_fp+"', sep='\\t')")
    r('anova <- aov('+formula+', data=d)')
    call = 'anova <- aov('+formula+', data=d)'
    r("capture.output(summary(anova), file='"+tmpfile+"')")
    #r("o = capture.output(summary(anova))")
    with open(tmpfile, "r") as f:
        o = []
        o.append(call)
        o.append('\n')
        for i,l in enumerate(f.readlines()):
            o.append(l)
    return o#, call


def manovaR(data_fp = 'data/PCs.tsv',
            formula = 'cbind(PC1, PC2, PC3)~ Group', groups='all'):
    '''
    Table has to be saved on disk, since R reads it from there.
    Can be changed to convert from pandas but...
    '''
    tmpfile='.tmpvacou'
    r("data <- read.csv('"+data_fp+"', sep='\\t')") #, check.names=FALSE)")

    if groups !='all':
        assert len(groups) == 2
        r("data <- droplevels(data[data$Group %in% c('"+ groups[0] +"', '"+groups[1]+"'),])")

    r('ma <- manova('+formula+', data=data)')
    call = 'ma <- manova('+formula+', data=data)'

    r("capture.output(summary(ma), file='"+tmpfile+"')")
    with open(tmpfile, "r") as f:
        o = {}
        o['summary'] = []
        o['summary.aov'] = []
        o['summary'].append(call)
        o['summary'].append('\n')
        o['summary.aov'].append(call)
        o['summary.aov'].append('\n')
        for i,l in enumerate(f.readlines()):
            o['summary'].append(l.rstrip())

    r("capture.output(summary.aov(ma), file='"+tmpfile+"')")
    with open(tmpfile, "r") as f:
        for i,l in enumerate(f.readlines()):
            o['summary.aov'].append(l.rstrip())

    return o#, call

def manovaR_results2table(results):
    all_tables = []

    z = results['summary']
    z = [s.replace('num Df', 'num_Df') for s in z] #TODO hacky
    tables_titles = ['Ovarall']
    tables = [z[i:i+3] for i in np.arange(0+2,len(z), 5)]
    tables = [pd.read_fwf(StringIO('\n'.join(s))) for s in tables]
    [t.insert(0, title, title) for title, t in zip(tables_titles,tables)]
    all_tables = all_tables + tables

    z = results['summary.aov']
    tables_titles = [z[i].strip(' :') for i in np.arange(0+2,len(z), 5)]
    tables = [z[i:i+3] for i in np.arange(0+3,len(z), 5)]
    tables = [pd.read_fwf(StringIO('\n'.join(s))) for s in tables]
    [t.insert(0, title, title) for title, t in zip(tables_titles,tables)]
    all_tables = all_tables + tables

    #all_tables = [t.reset_index(drop=True) for t in all_tables]
    all_tables = [t.rename(columns={'Unnamed: 0':'-'}) for t in all_tables]
    all_tables = [t.applymap(lambda x: '-' if pd.isnull(x) else x) for t in all_tables]

    return all_tables


def manova_contrastsR(data_fp = 'data/PCs.tsv',
                    formula = 'cbind(PC1, PC2, PC3)~ Group'):
    tmpfile='.tmpvacou'
    r("pcs <- read.csv('"+data_fp+"', sep='\\t')")

    r('ma <- manova('+formula+', data=pcs, contrasts = contrasts(pcs$Group))')
    call = 'ma <- manova('+formula+', data=pcs)'

    r("capture.output(summary(ma), file='"+tmpfile+"')")
    with open(tmpfile, "r") as f:
        o = {}
        o['summary'] = []
        o['summary.aov'] = []
        o['summary'].append(call)
        o['summary'].append('\n')
        o['summary.aov'].append(call)
        o['summary.aov'].append('\n')
        for i,l in enumerate(f.readlines()):
            o['summary'].append(l.rstrip())

    r("capture.output(summary.aov(ma), file='"+tmpfile+"')")
    with open(tmpfile, "r") as f:
        for i,l in enumerate(f.readlines()):
            o['summary.aov'].append(l.rstrip())
    return o#, call

def compare_groups():
    '''
    https://www.coursera.org/learn/machine-learning-data-analysis/lecture/XJJz2/running-a-k-means-cluster-analysis-in-python-pt-2

    Significance for group difference.
    1)  a) PCA, take all coponents (but can assume independence - ?; what about normality?)
        b) Factorial analysis of variance
            >1 groups, >1 measurements; requires independent variables
            main effects, interaction effects

    2) (M)ANOVA?
        http://www.math.wustl.edu/~victor/classes/ma322/r-eg-12.txt
        a) test for outliers (susceptible) - at least graphically
            R > mshapiro.test() from mvnormtest package - test for multivariate normality
            treating nvasc coupling variable as x, too?
    '''
    pass

