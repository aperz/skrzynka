from setuptools import setup

def readme():
    with open('README.rst') as f:
        return f.read()

setup(
    name='skrzynka',
    version='0.1',
    description='Various handy tools.',
    url='http://gitlab.com/aperz/skrzynka',
    author='Aleksandra Perz',
    author_email='',
    license='',
    packages=['skrzynka'],

    install_requires=[
            "pandas",
            "numpy",
        ],

    zip_safe=False
    )

